package com.ybg.report.datatable.domain;
import java.io.Serializable;

/** 数据保存实体 **/
public class DataTableDO implements Serializable {
	
	// String id;
	/** excel 第几行 0标识第一回 **/
	Integer	rrow;
	/** excel 第几列 0标识第一列 **/
	Integer	rcolumn;
	/** 值 **/
	String	rvalue;
	/** 部门编码 **/
	String	departmentno;
	String	gmt_create;
	String	gmt_modified;
	
	// public String getId() {
	// return id;
	// }
	//
	// public void setId(String id) {
	// this.id = id;
	// }
	public Integer getRrow() {
		return rrow;
	}
	
	public void setRrow(Integer rrow) {
		this.rrow = rrow;
	}
	
	public Integer getRcolumn() {
		return rcolumn;
	}
	
	public void setRcolumn(Integer rcolumn) {
		this.rcolumn = rcolumn;
	}
	
	public String getRvalue() {
		return rvalue;
	}
	
	public void setRvalue(String rvalue) {
		this.rvalue = rvalue;
	}
	
	public String getDepartmentno() {
		return departmentno;
	}
	
	public void setDepartmentno(String departmentno) {
		this.departmentno = departmentno;
	}
	
	public String getGmt_create() {
		return gmt_create;
	}
	
	public void setGmt_create(String gmt_create) {
		this.gmt_create = gmt_create;
	}
	
	public String getGmt_modified() {
		return gmt_modified;
	}
	
	public void setGmt_modified(String gmt_modified) {
		this.gmt_modified = gmt_modified;
	}
}
