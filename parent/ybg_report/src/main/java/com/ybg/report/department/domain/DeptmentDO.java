package com.ybg.report.department.domain;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21 */
@ApiModel("实体（数据库)")
public class DeptmentDO implements Serializable {
	
	private static final long	serialVersionUID	= 1L;
	/** 主键 **/
	@ApiModelProperty(name = "id", dataType = "java.lang.String", value = "主键", hidden = false)
	private String				id;
	/** 单位代码（唯一） **/
	@ApiModelProperty(name = "deptno", dataType = "java.lang.String", value = "单位代码（唯一）", hidden = false)
	private String				deptno;
	/** 父级ID **/
	@ApiModelProperty(name = "parentid", dataType = "java.lang.String", value = "父级ID", hidden = false)
	private String				parentid;
	/** 单位名称 **/
	@ApiModelProperty(name = "deptname", dataType = "java.lang.String", value = "单位名称", hidden = false)
	private String				deptname;
	/****/
	@ApiModelProperty(name = "gmtCreate", dataType = "java.lang.String", value = "", hidden = false)
	private String				gmtCreate;
	/****/
	@ApiModelProperty(name = "gmtModified", dataType = "java.lang.String", value = "", hidden = false)
	private String				gmtModified;
	/** 是否删除 **/
	@ApiModelProperty(name = "ifdel", dataType = "java.lang.Integer", value = "是否删除", hidden = false)
	private Integer				ifdel;
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setDeptno(String deptno) {
		this.deptno = deptno;
	}
	
	public String getDeptno() {
		return deptno;
	}
	
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	
	public String getParentid() {
		return parentid;
	}
	
	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}
	
	public String getDeptname() {
		return deptname;
	}
	
	public void setGmtCreate(String gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	public String getGmtCreate() {
		return gmtCreate;
	}
	
	public void setGmtModified(String gmtModified) {
		this.gmtModified = gmtModified;
	}
	
	public String getGmtModified() {
		return gmtModified;
	}
	
	public void setIfdel(Integer ifdel) {
		this.ifdel = ifdel;
	}
	
	public Integer getIfdel() {
		return ifdel;
	}
}
