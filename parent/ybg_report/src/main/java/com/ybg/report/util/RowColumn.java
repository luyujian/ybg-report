package com.ybg.report.util;
/** 行列
 * 
 * @author 严宇
 * @date 2018年1月18日
 * @version 1.0.0 */
public class RowColumn {
	
	int		row;
	int		column;
	Object	value;
	
	public int getRow() {
		return row;
	}
	
	public void setRow(int row) {
		this.row = row;
	}
	
	public int getColumn() {
		return column;
	}
	
	public void setColumn(int column) {
		this.column = column;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "RowColumn [row=" + row + ", column=" + column + ", value=" + value + "]";
	}
	
}
