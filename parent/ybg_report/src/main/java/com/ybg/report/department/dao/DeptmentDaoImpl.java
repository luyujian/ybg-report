package com.ybg.report.department.dao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.ybg.base.jdbc.BaseDao;
import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.jdbc.util.QvoConditionUtil;
import com.ybg.base.util.Page;
import com.ybg.report.department.domain.DeptmentVO;
import com.ybg.report.department.qvo.DeptmentQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import com.ybg.base.jdbc.DataBaseConstant;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

/** @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21 */
@Repository
public class DeptmentDaoImpl extends BaseDao implements DeptmentDao {
	
	@Autowired
	/** 注入你需要的数据库 **/
	@Qualifier(DataBaseConstant.JD_REPORT)
	JdbcTemplate jdbcTemplate;
	
	@Override
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	private static String	QUERY_TABLE_COLUMN	= "  deptment.deptno, deptment.parentid, deptment.deptname, deptment.gmt_create, deptment.gmt_modified, deptment.ifdel, deptment.id";
	private static String	QUERY_TABLE_NAME	= "report_deptment  deptment";
	
	@Override
	public DeptmentVO save(DeptmentVO deptment) throws Exception {
		BaseMap<String, Object> createmap = new BaseMap<String, Object>();
		String id = null;
		createmap.put("deptno", deptment.getDeptno());
		createmap.put("parentid", deptment.getParentid());
		createmap.put("deptname", deptment.getDeptname());
		createmap.put("gmt_create", deptment.getGmtCreate());
		createmap.put("gmt_modified", deptment.getGmtModified());
		createmap.put("ifdel", deptment.getIfdel());
		id = baseCreate(createmap, "report_deptment", "id");
		deptment.setId((String) id);
		return deptment;
	}
	
	@Override
	public void update(BaseMap<String, Object> updatemap, BaseMap<String, Object> WHEREmap) {
		this.baseupdate(updatemap, WHEREmap, "report_deptment");
	}
	
	@Override
	public Page list(Page page, DeptmentQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		page.setTotals(queryForInt(sql));
		if (page.getTotals() > 0) {
			page.setResult(getJdbcTemplate().query(page.getPagesql(sql), new BeanPropertyRowMapper<DeptmentVO>(DeptmentVO.class)));
		}
		else {
			page.setResult(new ArrayList<DeptmentVO>());
		}
		return page;
	}
	
	private String getcondition(DeptmentQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(WHERE).append("1=1");
		sqlappen(sql, "deptment.id", qvo.getId());
		sqlappen(sql, "deptment.deptno", qvo.getDeptno());
		sqlappen(sql, "deptment.parentid", qvo.getParentid());
		sqlappen(sql, "deptment.deptname", qvo.getDeptname());
		sqlappen(sql, "deptment.gmt_create", qvo.getGmtCreate());
		sqlappen(sql, "deptment.gmt_modified", qvo.getGmtModified());
		sqlappen(sql, "deptment.ifdel", qvo.getIfdel());
		return sql.toString();
	}
	
	@Override
	public List<DeptmentVO> list(DeptmentQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		return getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<DeptmentVO>(DeptmentVO.class));
	}
	
	@Override
	public void remove(BaseMap<String, Object> wheremap) {
		baseremove(wheremap, "report_deptment");
	}
	
	@Override
	public DeptmentVO get(String id) {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(WHERE).append("1=1");
		sql.append(AND).append("id='" + id + "'");
		List<DeptmentVO> list = getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<DeptmentVO>(DeptmentVO.class));
		return QvoConditionUtil.checkList(list) ? list.get(0) : null;
	}
}
