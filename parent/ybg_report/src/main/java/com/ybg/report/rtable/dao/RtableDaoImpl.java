package com.ybg.report.rtable.dao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import com.ybg.base.jdbc.BaseDao;
import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.jdbc.util.QvoConditionUtil;
import com.ybg.base.util.Page;
import com.ybg.report.rtable.domain.RtableVO;
import com.ybg.report.rtable.qvo.RtableQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import com.ybg.base.jdbc.DataBaseConstant;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

/** @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-22 */
@Repository
public class RtableDaoImpl extends BaseDao implements RtableDao {
	
	@Autowired
	/** 注入你需要的数据库 **/
	@Qualifier(DataBaseConstant.JD_REPORT)
	JdbcTemplate jdbcTemplate;
	
	@Override
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	private static String	QUERY_TABLE_COLUMN	= " rtable.storetablename , rtable.reportno, rtable.reportname, rtable.reportyear, rtable.gmt_create, rtable.gmt_modified, rtable.dataarea, rtable.tempid, rtable.id";
	private static String	QUERY_TABLE_NAME	= "report_rtable  rtable";
	
	@Override
	public RtableVO save(RtableVO rtable) throws Exception {
		BaseMap<String, Object> createmap = new BaseMap<String, Object>();
		String id = null;
		createmap.put("reportno", rtable.getReportno());
		createmap.put("reportname", rtable.getReportname());
		createmap.put("reportyear", rtable.getReportyear());
		createmap.put("dataarea", rtable.getDataarea());
		createmap.put("tempid", rtable.getTempid());
		createmap.put("storetablename", rtable.getStoretablename());
		id = baseCreate(createmap, "report_rtable", "id");
		rtable.setId((String) id);
		return rtable;
	}
	
	@Override
	public void update(BaseMap<String, Object> updatemap, BaseMap<String, Object> WHEREmap) {
		this.baseupdate(updatemap, WHEREmap, "report_rtable");
	}
	
	@Override
	public Page list(Page page, RtableQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		page.setTotals(queryForInt(sql));
		if (page.getTotals() > 0) {
			page.setResult(getJdbcTemplate().query(page.getPagesql(sql), new BeanPropertyRowMapper<RtableVO>(RtableVO.class)));
		}
		else {
			page.setResult(new ArrayList<RtableVO>());
		}
		return page;
	}
	
	private String getcondition(RtableQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(WHERE).append("1=1");
		sqlappen(sql, "rtable.id", qvo.getId());
		sqlappen(sql, "rtable.reportno", qvo.getReportno());
		sqlappen(sql, "rtable.reportname", qvo.getReportname());
		sqlappen(sql, "rtable.reportyear", qvo.getReportyear());
		sqlappen(sql, "rtable.dataarea", qvo.getDataarea());
		sqlappen(sql, "rtable.tempid", qvo.getTempid());
		sqlappen(sql, "rtable.storetablename", qvo.getStoretablename());
		return sql.toString();
	}
	
	@Override
	public List<RtableVO> list(RtableQuery qvo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(getcondition(qvo));
		return getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<RtableVO>(RtableVO.class));
	}
	
	@Override
	public void remove(BaseMap<String, Object> wheremap) {
		baseremove(wheremap, "report_rtable");
	}
	
	@Override
	public RtableVO get(String id) {
		StringBuilder sql = new StringBuilder();
		sql.append(SELECT).append(QUERY_TABLE_COLUMN).append(FROM).append(QUERY_TABLE_NAME);
		sql.append(WHERE).append("1=1");
		sql.append(AND).append("id='" + id + "'");
		List<RtableVO> list = getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper<RtableVO>(RtableVO.class));
		return QvoConditionUtil.checkList(list) ? list.get(0) : null;
	}
	
	@Override
	public void createDataTable(String datatablename) {
		StringBuilder sql = new StringBuilder();
		sql.append("CREATE TABLE `").append(datatablename).append("` (");
		sql.append(" `id`  varchar(64) NOT NULL ,");
		sql.append(" `rrow`  int NULL ,");
		sql.append(" `rcolumn`  int NULL ,");
		sql.append(" `gmt_create`  datetime NULL ,");
		sql.append(" `gmt_modified`  datetime NULL ,");
		sql.append(" `rvalue`  varchar(64) NULL ,");
		sql.append(" `departmentno`  varchar(64) NULL ,");
		sql.append(" PRIMARY KEY (`id`),");
		sql.append(" UNIQUE INDEX (`rrow`, `rcolumn`, `rvalue`, `departmentno`) ");
		sql.append(" );");
		try {
			getJdbcTemplate().update(sql.toString());
		} catch (Exception e) {
			//如果已经存在了 就报错就可以了
			e.printStackTrace();
		}
	}
}
